async function notifyOnSlack(authorization) {
  const response = await fetch(process.env.slackUrl, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ text: `Hello World! I just simulated a transaction with ${authorization.merchant.name} for an amount of ${authorization.currencyCode} ${helpers.format.decimal(authorization.centsAmount / 100, 100)}. It's a great day to be alive. Love, <@${process.env.slackID}> - auth result: ${authorization.output}`}),
  });
};

// START ACCOUNTABILITY PARTNER NOTIFIER //

async function notifyAccountabilityPartner() {
  const response = await fetch("https://api.telegram.org/bot1217002378:AAG3hz2-QqUd7W6i9lxf2BvROlf6uLDQo8g/sendMessage", {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ chat_id:`668012899`, text:`We will have to talk about your spending behaviours later when I get home...`}),
  });
};

// END ACCOUNTABILITY PARTNER NOTIFIER //


// START SPEND JUDGER //

var spendingMessage = "Normal amounts of spending detected.";

async function spendJudger(authorization) {
    if(authorization.centsAmount >= 500000){
        spendingMessage = "Warning: Abnormal spending behaviour detected... Girlfriend notified!";
        //notifyAccountabilityPartner();
    };
};

async function notifyPushed(authorization) {
  const response = await fetch(process.env.pushed_url, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ app_key:`${process.env.pushed_app_key}`, app_secret:`${process.env.pushed_app_secret}`, target_type:`pushed_id`, pushed_id:`${process.env.pushed_pushed_id}`, content:`You just spent R${helpers.format.decimal(authorization.centsAmount / 100, 100)}. ${spendingMessage}` }),
  });
};

// END SPEND JUDGER //

// This function runs before a transaction.
const beforeTransaction = async (authorization) => {
    await spendJudger(authorization);
    await notifyPushed(authorization);
      // This is an example of how to decline a transaction based on the transaction payload.
      if(authorization.merchant.name == "Fish and Chips Junction"){
          return false // Decline the transaction
      } else {
          return true // Authorise the transaction
      }
};
// This function runs after a transaction was successful.
const afterTransaction = async (transaction) => {
  //await notifyOnSlack(transaction);
  console.log(authorization);
};
// This function runs after a transaction has been declined.
const afterDecline = async (transaction) => { 

};
// This function runs after a transaction has been reversed.
const afterReversal = async (transaction) => { 
    
};
// This function runs after a transaction has been adjusted.
// const afterAdjustment = async (transaction) => { };